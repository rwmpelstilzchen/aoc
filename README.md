# Advent of Code

I began participating in [AoC](https://adventofcode.com/) in 2023, in order to learn some Rust. My code is as far from exemplary as one could imagine, so please do not learn anything from it… 😁
