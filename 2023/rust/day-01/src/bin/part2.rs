use std::fs::read_to_string;
use regex::Regex;
use aho_corasick::AhoCorasick;

fn read_input() -> Vec<String> {
    let mut result = Vec::new();
    for line in read_to_string("input").unwrap().lines() {
        result.push(line.to_string())
    }
    result
}

fn convert_numbers(lines: Vec<String>) -> Vec<String> {
    let patterns = &["one", "two", "three", "four", "five", "six", "seven", "eight", "nine"];
    let replace_with = &["1", "2", "3", "4", "5", "6", "7", "8", "9"];
    let mut result = Vec::new();
    for line in lines {
        let ac = AhoCorasick::new(patterns);
        result.push(ac.unwrap().replace_all(&line, replace_with));
    }
    result
}

fn sum_calibration_values(lines: Vec<String>) -> i32 {
    let re_first = Regex::new(r"^\D*(\d)").unwrap();
    let re_last = Regex::new(r"(\d)\D*$").unwrap();
    let mut sum = 0;
    for line in lines {
        let Some(first) = re_first.captures(&line) else { return -1 };
        let Some(last) = re_last.captures(&line) else { return -1 };
        let calibration_value = [&first[1], &last[1]].join("").parse::<i32>().unwrap();
        sum = sum + calibration_value;
    }
    sum
}

fn main() {
    let lines = read_input();
    let converted_lines = convert_numbers(lines);
    println!("{}", sum_calibration_values(converted_lines));
}
