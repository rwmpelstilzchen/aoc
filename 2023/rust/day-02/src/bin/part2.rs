use std::fs::read_to_string;
use regex::Regex;
use std::cmp;

fn read_input() -> Vec<String> {
    let mut result = Vec::new();
    for line in read_to_string("input").unwrap().lines() {
        result.push(line.to_string())
    }
    result
}

fn the_game_itself(game: String) -> String {
    let re_the_game_itself = Regex::new(r"^Game \d*: (.*)").unwrap();
    let Some(the_game_itself) = re_the_game_itself.captures(&game) else { panic!() };
    return String::from(&the_game_itself[1]);
}

fn game_power(game: String) -> i32 {
    let subgames = game.split("; ");
    let mut max_red   = 0;
    let mut max_green = 0;
    let mut max_blue  = 0;
    for subgame in subgames {
        for coloured_cubes in subgame.split(", ") {
            let (cubes, colour) = coloured_cubes.split_once(" ").unwrap();
            match colour {
                "red" => {max_red = cmp::max(max_red, cubes.parse::<i32>().unwrap())},
                "green" => {max_green = cmp::max(max_green, cubes.parse::<i32>().unwrap())},
                "blue" => {max_blue = cmp::max(max_blue, cubes.parse::<i32>().unwrap())},
                _ => panic!()
            }
        }
    }
    max_red * max_green * max_blue
}

fn main() {
    let games = read_input();
    let mut sum = 0;
    for game in games {
        sum += game_power(the_game_itself(game));
    }
    println!("{}", sum);
}
