use std::fs::read_to_string;
use regex::Regex;

const RED_CUBES:   i32 = 12;
const GREEN_CUBES: i32 = 13;
const BLUE_CUBES:  i32 = 14;

fn read_input() -> Vec<String> {
    let mut result = Vec::new();
    for line in read_to_string("input").unwrap().lines() {
        result.push(line.to_string())
    }
    result
}

fn game_id(game: String) -> i32 {
    let re_game_id = Regex::new(r"^Game (\d*):").unwrap();
    let Some(game_id) = re_game_id.captures(&game) else { return -1 };
    return game_id[1].parse::<i32>().unwrap();
}

fn the_game_itself(game: String) -> String {
    let re_the_game_itself = Regex::new(r"^Game \d*: (.*)").unwrap();
    let Some(the_game_itself) = re_the_game_itself.captures(&game) else { panic!() };
    return String::from(&the_game_itself[1]);
}

fn is_subgame_possible(subgame: String) -> bool {
    for coloured_cubes in subgame.split(", ") {
        let (cubes, colour) = coloured_cubes.split_once(" ").unwrap();
        match colour {
            "red" => {if cubes.parse::<i32>().unwrap() > RED_CUBES {return false}},
            "green" => {if cubes.parse::<i32>().unwrap() > GREEN_CUBES {return false}},
            "blue" => {if cubes.parse::<i32>().unwrap() > BLUE_CUBES {return false}},
            _ => panic!()
        }
    }
    return true;
}

fn is_game_possible(game: String) -> bool {
    let subgames = game.split("; ");
    for subgame in subgames {
        if !is_subgame_possible(String::from(subgame)) {
            return false;
        }
    }
    true
}

fn main() {
    let games = read_input();
    let mut sum = 0;
    for game in games {
        if is_game_possible(the_game_itself(game.clone())) {
            sum = sum + game_id(game);
        }
    }
    println!("{}", sum);
}
